.PHONY: clean watch

TEX=$(shell find . -name '*.tex')
DEPS=$(shell find . -name '*.cls')
PDF=$(patsubst %.tex,%.pdf,${TEX})

all: ${PDF}

%.pdf: %.tex ${DEPS}
	(cd $(dir $<); pdflatex $(notdir $<))

watch:
	./watch.sh

clean:
	rm -f \
		$(shell find . -iname '*.pdf') \
		$(shell find . -iname '*.log') \
		$(shell find . -iname '*.aux')
